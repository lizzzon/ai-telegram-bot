class TestFilters:
    @staticmethod
    def test__filter():
        sender_id = 1234
        users_id = [245, 2342, 131244, 113, 1234]
        assert (sender_id in users_id) == True
