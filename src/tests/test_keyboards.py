from aiogram import types
from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup, KeyboardButton, ReplyKeyboardMarkup
from aiogram.utils.keyboard import InlineKeyboardBuilder


class TestKeyboardGeneratorsType:

    @staticmethod
    def test_generate_url_button():
        url_button: list[list[InlineKeyboardButton]] = [[InlineKeyboardButton(text='web', url='url')]]
        keyboard: InlineKeyboardMarkup = InlineKeyboardMarkup(inline_keyboard=url_button)
        assert type(keyboard) == InlineKeyboardMarkup

    @staticmethod
    def test_generate_reply_keyboard():
        keyboard: ReplyKeyboardMarkup = ReplyKeyboardMarkup(
            keyboard=[[button for button in [
                KeyboardButton(text='button1'),
                KeyboardButton(text='button2'),
            ]]],
            resize_keyboard=True)
        assert type(keyboard) == ReplyKeyboardMarkup

    @staticmethod
    def test_generate_inline_keyboard():
        keyboard: InlineKeyboardBuilder = InlineKeyboardBuilder()

        for el in ['button1', 'button2']:
            keyboard.add(types.InlineKeyboardButton(
                text=el,
                callback_data=el)
            )

        keyboard.adjust(2)
        assert type(keyboard.as_markup()) == InlineKeyboardMarkup
