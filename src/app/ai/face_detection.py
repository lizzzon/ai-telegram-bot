import tempfile

from PIL import Image

from config.settings import RoboflowSettings


class PhotoAnalyser:
    """Class consist methods to analyse user`s photo."""

    def __init__(self, photo: Image) -> None:
        """Set photo param."""
        self.photo = Image.open(photo)
        self.temp: list[str] = []

    def analyse_photo(self) -> str:
        """Return string of persons on photo."""
        with tempfile.NamedTemporaryFile(suffix='.jpg', delete=False) as tmp:
            temp_file_path: str = tmp.name
            self.photo.save(tmp.name)

        for prediction in RoboflowSettings.model.predict(
                temp_file_path,
                confidence=40,
                overlap=30
        ).json()['predictions']:
            self.temp.append(prediction['class'])

        return '\n'.join(self.temp)

    def return_analysed_photo(self) -> str:
        """Return analysed photo of persons."""
        with tempfile.NamedTemporaryFile(suffix='.jpg', delete=False) as tmp:
            temp_file_path: str = tmp.name
            self.photo.save(tmp.name)
        RoboflowSettings.model.predict(temp_file_path, confidence=40, overlap=30).save(
            f'{temp_file_path}_prediction.jpg'
        )

        return f'{temp_file_path}_prediction.jpg'

    def add_more_persons(self, text: str) -> str:
        """Add new person to list."""
        if text not in self.temp:
            self.temp.append(text)
            return '\n'.join(self.temp)
        return 'The system has already add this person to list before'

    def delete_persons(self, text: str) -> str:
        """Delete person from list."""
        if text in self.temp:
            self.temp.remove(text)
            return '\n'.join(self.temp)
        return 'The system did not add this person to the list before'
