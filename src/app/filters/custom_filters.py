from aiogram.filters import BaseFilter
from aiogram.types import Message


class IsAdmin(BaseFilter):
    """Filter class to check is the user an admin."""

    def __init__(self, admin_id: list[int]) -> None:
        """Assign admin_id. Accepts a list of admins from config data."""
        self.admin_id = admin_id

    async def __call__(self, message: Message) -> bool:
        """Return true if sender_id in admin_id list."""
        return message.from_user.id in self.admin_id


class IsRegister(BaseFilter):
    """Filter class to check is the user registered."""

    def __init__(self, users_id: list[int]) -> None:
        """Assign user_id. Accepts a list of users."""
        self.users_id = users_id

    async def __call__(self, message: Message) -> bool:
        """Return true if sender_id in user_id list."""
        return message.from_user.id in self.users_id


class IsTeacher(BaseFilter):
    """Filter class to check is the user a teacher."""

    def __init__(self, teachers_id: list[int]) -> None:
        """Assign teacher_id. Accepts a list of teachers."""
        self.teachers_id = teachers_id

    async def __call__(self, message: Message) -> bool:
        """Return true if sender_id in teacher_id list."""
        return message.from_user.id in self.teachers_id


class IsStudent(BaseFilter):
    """Filter class to check is the user a student."""

    def __init__(self, students_id: list[int]) -> None:
        """Assign student_id. Accepts a list of students."""
        self.students_id = students_id

    async def __call__(self, message: Message) -> bool:
        """Return true if sender_id in student_id list."""
        return message.from_user.id in self.students_id
