class RegularExpressions:
    latin_and_spaces = r"^[a-zA-Z\s]+$"
    latin_spaces_commas = r"^[a-zA-Z, ]+$"
    plus_digits = r"^\+?\d+$"
