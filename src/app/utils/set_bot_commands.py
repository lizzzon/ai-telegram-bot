from aiogram import Bot
from aiogram.types import BotCommand, BotCommandScopeDefault


class SetUserCommand:
    """Class contains a list of user commands; a static method for adding them to the telegram template menu."""

    user_data: list[tuple[list[BotCommand], BotCommandScopeDefault, None]] = [
        (
            [BotCommand(command="start", description="Register")],
            BotCommandScopeDefault(),
            None,
        )
    ]

    @staticmethod
    async def set_user_commands(bot: Bot) -> None:
        """Set user(begin) command to telegram menu template."""
        for commands_list, commands_scope, language in SetUserCommand.user_data:
            await bot.set_my_commands(
                commands=commands_list, scope=commands_scope, language_code=language
            )


class SetUserMenuCommand:
    """Class user menu commands."""

    user_menu_data: list[tuple[list[BotCommand], BotCommandScopeDefault, None]] = [
        (
            [BotCommand(command="menu", description="Show user menu")],
            BotCommandScopeDefault(),
            None,
        )
    ]

    @staticmethod
    async def set_user_menu_commands(bot: Bot) -> None:
        """Set user menu command to telegram menu template."""
        for (
            commands_list,
            commands_scope,
            language,
        ) in SetUserMenuCommand.user_menu_data:
            await bot.set_my_commands(
                commands=commands_list, scope=commands_scope, language_code=language
            )


class SetAdminCommand:
    """Class contains a list of admin commands; a static method for adding them to the telegram template menu."""

    admin_data: list[tuple[list[BotCommand], BotCommandScopeDefault, None]] = [
        (
            [BotCommand(command="admin_menu", description="Show admin menu")],
            BotCommandScopeDefault(),
            None,
        )
    ]

    @staticmethod
    async def set_admin_commands(bot: Bot) -> None:
        """Set admin command to telegram menu template."""
        for commands_list, commands_scope, language in SetAdminCommand.admin_data:
            await bot.set_my_commands(
                commands=commands_list, scope=commands_scope, language_code=language
            )


class SetTeacherCommand:
    """Class contains a list of teacher commands; a static method for adding them to the telegram template menu."""

    teacher_data: list[tuple[list[BotCommand], BotCommandScopeDefault, None]] = [
        (
            [BotCommand(command="teacher_menu", description="Show teacher menu")],
            BotCommandScopeDefault(),
            None,
        )
    ]

    @staticmethod
    async def set_teacher_commands(bot: Bot) -> None:
        """Set teacher command to telegram menu template."""
        for commands_list, commands_scope, language in SetTeacherCommand.teacher_data:
            await bot.set_my_commands(
                commands=commands_list, scope=commands_scope, language_code=language
            )


class SetStudentCommand:
    """Class contains a list of student commands; a static method for adding them to the telegram template menu."""

    student_data: list[tuple[list[BotCommand], BotCommandScopeDefault, None]] = [
        (
            [BotCommand(command="student_menu", description="Show student menu")],
            BotCommandScopeDefault(),
            None,
        )
    ]

    @staticmethod
    async def set_student_commands(bot: Bot) -> None:
        """Set student command to telegram menu template."""
        for commands_list, commands_scope, language in SetStudentCommand.student_data:
            await bot.set_my_commands(
                commands=commands_list, scope=commands_scope, language_code=language
            )
