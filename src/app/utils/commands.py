BOT_COMMANDS: list[tuple[str, str]] = [
    ('info', 'Info about your account'),
    ('role', 'Choose role'),
    ('menu', 'Show user menu'),
    ('admin', 'Go to admin panel'),
    ('teacher', 'Go to teacher panel'),
    ('student', 'Go to student panel')
]

ADMIN_COMMANDS: list[tuple[str, str]] = [
    ('admin_menu', 'Show admin commands'),
    ('users', 'Show all usernames'),
    ('back to user', 'Return to user chat')
]

TEACHER_COMMANDS: list[tuple[str, str]] = [
    ('analyse', 'Analyse photo'),
    ('cabinet', 'Your cabinet'),
    ('back to user', 'Return to user chat')
]

STUDENT_COMMANDS: list[tuple[str, str]] = [
    ('cabinet', 'Your cabinet'),
    ('back to user', 'Return to user chat')
]
