import re

from aiogram import Bot, Router
from aiogram.filters import Command, Text
from aiogram.fsm.context import FSMContext
from aiogram.types import (
    CallbackQuery,
    Message,
    ReplyKeyboardMarkup,
    ReplyKeyboardRemove,
)
from utils.regular import RegularExpressions

from app.config.settings import DatabaseSettings
from app.fsm.fsm import StudentRegister
from app.keyboards.keyboard import ButtonsKeyboard, MenuKeyboard
from app.models.models import Roles, Student, User
from app.utils.commands import STUDENT_COMMANDS
from app.utils.set_bot_commands import SetStudentCommand

router: Router = Router()


@router.callback_query(Text(text="Student👩‍🎓"))
async def process_switch_student_command(callback: CallbackQuery, bot: Bot) -> None:
    """Redirect to the student chat."""
    for user in DatabaseSettings.session.query(User).all():
        if callback.from_user.id == user.id and user.role is not None:
            if user.role == Roles.student:
                await bot.delete_my_commands()
                await SetStudentCommand.set_student_commands(bot)
                await callback.message.answer(
                    "Switch to student\nNew options available\n",
                    reply_markup=ReplyKeyboardRemove(),
                )
            else:
                await callback.message.answer("You are a teacher")

    await callback.answer()


@router.message(lambda msg: msg.text == "/student_menu", Command("student_menu"))
async def set_student_menu(message: Message) -> None:
    """Generate and show student menu."""
    keyboard: ReplyKeyboardMarkup = ButtonsKeyboard.generate_reply_keyboard(
        MenuKeyboard.student_commands_menu
    )
    await message.answer(text="Your available menu", reply_markup=keyboard)


@router.message(Text(text="Help😫"))
async def process_help_student_command(message: Message) -> None:
    """Execute command: student help. Show available student commands."""
    await message.answer(
        "\n".join(
            [
                f"{commands} - {description}"
                for commands, description in STUDENT_COMMANDS
            ]
        )
    )


@router.message(Text(text="Add info"))
async def process_register_student(message: Message, state: FSMContext) -> None:
    """Execute command: add info. Get info from user using FMS."""
    await state.set_state(StudentRegister.name)

    await message.answer("Enter your full name")


@router.message(Text(text="Show full info"))
async def process_show_full_info(message: Message) -> None:
    """Show student info."""
    for student in DatabaseSettings.session.query(Student).all():
        if student.user_id == message.from_user.id:
            await message.answer(
                f"Your full info:\n"
                f"Name – {student.student_name}\n"
                f"Telephone – {student.telephone}\n"
                f"Address – {student.address}"
            )
    await message.answer("If you want to change info, press the button - Add info")


@router.message(StudentRegister.name)
async def process_name(message: Message, state: FSMContext) -> None:
    """Get user`s name."""
    if re.match(RegularExpressions.latin_and_spaces, message.text):
        await state.update_data(name=message.text)

        DatabaseSettings.session.query(Student).filter(
            Student.user_id == message.from_user.id
        ).update(
            {
                Student.student_name: " ".join(
                    [el.capitalize() for el in message.text.split()]
                )
            }
        )
        DatabaseSettings.session.commit()
    else:
        await message.answer("Enter correct value")
        return

    await state.set_state(StudentRegister.telephone)

    await message.answer("Enter your telephone")


@router.message(StudentRegister.telephone)
async def process_telephone(message: Message, state: FSMContext) -> None:
    """Get user`s telephone."""
    if re.match(RegularExpressions.plus_digits, message.text):
        await state.update_data(telephone=message.text)

        DatabaseSettings.session.query(Student).filter(
            Student.user_id == message.from_user.id
        ).update({Student.telephone: message.text})
        DatabaseSettings.session.commit()
    else:
        await message.answer("Enter correct value")
        return

    await state.set_state(StudentRegister.address)

    await message.answer("Enter your address")


@router.message(StudentRegister.address)
async def process_address(message: Message, state: FSMContext) -> None:
    """Get user`s address."""
    await state.update_data(address=message.text)

    DatabaseSettings.session.query(Student).filter(
        Student.user_id == message.from_user.id
    ).update({Student.address: message.text})
    DatabaseSettings.session.commit()

    await state.clear()

    await message.answer("Success!")
