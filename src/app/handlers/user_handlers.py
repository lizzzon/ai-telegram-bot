from aiogram import Bot, Router
from aiogram.filters import CommandStart, Text
from aiogram.fsm.context import FSMContext
from aiogram.types import (CallbackQuery, InlineKeyboardMarkup, Message)

from app.config.settings import DatabaseSettings
from app.fsm.fsm import StartMenu
from app.keyboards.keyboard import ButtonsKeyboard, MenuKeyboard
from app.models.db_commands import StudentCommand, TeacherCommand, UpdateCommandUser, UserCommand
from app.models.models import Roles, Student, Teacher, User
from app.utils.commands import BOT_COMMANDS

router: Router = Router()


@router.message(lambda msg: msg.text == '/start', CommandStart())
async def process_start_command(message: Message, bot: Bot, state: FSMContext) -> None:
    """Execute registration if the user is not registered. Else sends a message about a re-registration attempt."""
    for user in DatabaseSettings.session.query(User).all():
        if message.from_user.id == user.id:
            await message.answer('You are already registered!')
            break

    if UserCommand.register_user(message):
        await message.answer('Success!')

    await state.set_state(StartMenu.menu)
    await bot.delete_my_commands()
    await message.answer('Now your available menu. Please, write command <menu>')


@router.message(StartMenu.menu)
async def set_user_menu(message: Message, state: FSMContext) -> None:
    """Generate and show user menu."""
    keyboard: InlineKeyboardMarkup = ButtonsKeyboard.generate_inline_keyboard(MenuKeyboard.user_commands_menu)
    await message.answer('*YOUR AVAILABLE MENU*', parse_mode='Markdown', reply_markup=keyboard)

    await state.clear()


@router.callback_query(Text(text='Role👩‍🏫'))
async def set_role_command(callback: CallbackQuery) -> None:
    """Execute role selection process. Generate inline button."""
    for user in DatabaseSettings.session.query(User).all():
        if callback.from_user.id == user.id:
            if user.role is None or user.admin:
                keyboard: InlineKeyboardMarkup = ButtonsKeyboard.generate_inline_keyboard(
                    MenuKeyboard.role_commands_menu
                )
                await callback.message.answer('*Choose role*', parse_mode='Markdown', reply_markup=keyboard)
            else:
                await callback.message.answer('You have already chosen a role')

    await callback.answer()


@router.callback_query(Text(text=['👩‍🏫Teacher🧑‍🏫']))
async def set_role_teacher(callback: CallbackQuery) -> None:
    """Update db column User.role(teacher)."""
    await callback.message.delete()

    UpdateCommandUser.update_table('teacher', callback.from_user.id)

    if str(callback.from_user.id) not in \
            [str(el).strip('(),') for el in DatabaseSettings.session.query(Teacher.user_id).all()]:
        TeacherCommand.add_teacher(callback.from_user.id, callback.from_user.first_name)

    # TODO: удаление по изменению ролей
    await callback.message.answer('You choose to be a teacher')

    await callback.answer()


@router.callback_query(Text(text=['👩‍🎓Student🧑‍🎓']))
async def set_role_student(callback: CallbackQuery) -> None:
    """Update db column User.role(student)."""
    await callback.message.delete()

    UpdateCommandUser.update_table('student', callback.from_user.id)

    if str(callback.from_user.id) not in \
            [str(el).strip('(),') for el in DatabaseSettings.session.query(Student.user_id).all()]:
        StudentCommand.add_student(callback.from_user.id)

    # TODO: удаление по изменению ролей
    await callback.message.answer('You choose to be a student')

    await callback.answer()


@router.callback_query(Text(text='Info📖'))
async def process_info_command(callback: CallbackQuery) -> None:
    """Execute command: user info. Show info about user from db."""
    for user in DatabaseSettings.session.query(User).all():
        role: str = 'teacher' if user.role == Roles.teacher else 'student'
        if user.id == callback.from_user.id:
            is_admin: str = 'admin' if user.admin else 'user'
            if user.role is not None:
                await callback.message.answer(f'Name: {user.name};\nRights: {is_admin};\nRole: {role}.')
            else:
                await callback.message.answer('Choose role for full info')

    await callback.answer()


@router.callback_query(Text(text='Help🚑'))
async def process_help_command(callback: CallbackQuery) -> None:
    """Execute command: user help. Show available user commands."""
    await callback.message.answer(''.join([f'{commands} - {description}\n' for commands, description in BOT_COMMANDS]))

    await callback.answer()


@router.callback_query(Text(text='Website🕸️'))
async def process_upload_command(callback: CallbackQuery) -> None:
    """Execute command: website. Show website link."""
    await callback.message.answer(
        '⬇️ Website link ⬇️',
        reply_markup=ButtonsKeyboard.generate_url_button('web', 'vk.com')
    )

    await callback.answer()
