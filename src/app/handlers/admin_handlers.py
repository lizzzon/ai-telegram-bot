from aiogram import Bot, Router
from aiogram.filters import Command, Text
from aiogram.types import CallbackQuery, Message, ReplyKeyboardMarkup

from app.config.settings import DatabaseSettings, admin_id
from app.filters.custom_filters import IsAdmin, IsRegister
from app.keyboards.keyboard import ButtonsKeyboard, MenuKeyboard
from app.models.models import User
from app.utils.commands import ADMIN_COMMANDS
from app.utils.set_bot_commands import SetAdminCommand

router: Router = Router()


@router.callback_query(Text(text='Admin🔐💅🏻'))
async def process_go_to_admin_command(callback: CallbackQuery, bot: Bot) -> None:
    """Redirect to the admin chat."""
    for user in DatabaseSettings.session.query(User).all():
        if callback.from_user.id == user.id:
            if user.admin:
                await bot.delete_my_commands()
                await SetAdminCommand.set_admin_commands(bot)
                await callback.message.answer('Switch to admin\nNew options available\n')
            else:
                await callback.message.answer('You dont have admin rights')

    await callback.answer()


@router.message(lambda msg: msg.text == '/admin_menu', Command('admin_menu'),
                IsRegister([x.id for x in DatabaseSettings.session.query(User.id).distinct()]), IsAdmin(admin_id))
async def set_admin_menu(message: Message) -> None:
    """Generate and show admin menu."""
    keyboard: ReplyKeyboardMarkup = ButtonsKeyboard.generate_reply_keyboard(MenuKeyboard.admin_commands_menu)
    await message.answer(text='Your available menu', reply_markup=keyboard)


@router.message(Text(text='Help🗿'))
async def process_help_admin_command(message: Message) -> None:
    """Execute command: admin help."""
    await message.answer('\n'.join([f'{commands} - {description}' for commands, description in ADMIN_COMMANDS]))


@router.message(Text(text='Users👩🏼‍💻'))
async def process_check_users_command(message: Message) -> None:
    """Execute command: show list of registered users."""
    await message.answer(',\n'.join(sorted([users.username for users in DatabaseSettings.session.query(User).all()])))
