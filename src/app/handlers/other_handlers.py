from aiogram import Bot, F, Router
from aiogram.enums import ContentType
from aiogram.filters import Text
from aiogram.types import Message, ReplyKeyboardRemove

from app.config.settings import DatabaseSettings
from app.filters.custom_filters import IsRegister, IsTeacher
from app.models.models import Teacher, User
from app.utils.set_bot_commands import SetUserCommand

router: Router = Router()


@router.message(Text(text="Back to user😥"))
async def process_back_to_user_command(message: Message, bot: Bot) -> None:
    """Redirect to the user chat."""
    await bot.delete_my_commands()
    await SetUserCommand.set_user_commands(bot)
    await message.answer("Switch to user", reply_markup=ReplyKeyboardRemove())


@router.message(
    F.content_type == ContentType.PHOTO,
    IsRegister([x.id for x in DatabaseSettings.session.query(User.id).distinct()]),
    IsTeacher(
        [
            int(str(el).strip("(),"))
            for el in DatabaseSettings.session.query(Teacher.user_id).all()
        ]
    ),
)
async def upload_photo(message: Message) -> None:
    """Accept a photo value type."""
    await message.answer("Photo uploaded")


@router.message(
    F.content_type.in_({"voice", "video", "video_note"}),
    IsRegister([x.id for x in DatabaseSettings.session.query(User.id).distinct()]),
)
async def upload_media(message: Message) -> None:
    """Accept a video or a voice value type."""
    if message.video:
        await message.answer("You uploaded a video")
    if message.voice:
        await message.answer("You uploaded a voice")
    if message.video_note:
        await message.answer("You upload video-note")


@router.message()
async def send_another(message: Message) -> None:
    """Work if another handler's haven't been executed."""
    if message.from_user.id in [
        x.id for x in DatabaseSettings.session.query(User.id).distinct()
    ]:
        if isinstance(message.text, str) and "/menu" not in message.text:
            await message.answer("You enter string")
    else:
        await message.answer("You should register firstly")
