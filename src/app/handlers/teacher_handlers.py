import re
from typing import Any, BinaryIO

from aiogram import Bot, F, Router
from aiogram.enums import ContentType
from aiogram.filters import Command, Text
from aiogram.fsm.context import FSMContext
from aiogram.types import (
    CallbackQuery,
    File,
    FSInputFile,
    InlineKeyboardMarkup,
    Message,
    ReplyKeyboardMarkup,
    ReplyKeyboardRemove,
)
from utils.regular import RegularExpressions

from app.ai.face_detection import PhotoAnalyser
from app.config.settings import DatabaseSettings
from app.fsm.fsm import TeacherRegister, UploadPhoto
from app.keyboards.keyboard import ButtonsKeyboard, MenuKeyboard
from app.models.models import Roles, Teacher, User
from app.utils.commands import TEACHER_COMMANDS
from app.utils.set_bot_commands import SetTeacherCommand

router: Router = Router()


@router.callback_query(Text(text="Teacher👩‍🏫"))
async def process_switch_teacher_command(callback: CallbackQuery, bot: Bot) -> None:
    """Redirect to the teacher chat."""
    for user in DatabaseSettings.session.query(User).all():
        if callback.from_user.id == user.id and user.role is not None:
            if user.role == Roles.teacher:
                await bot.delete_my_commands()
                await SetTeacherCommand.set_teacher_commands(bot)
                await callback.message.answer(
                    "Switch to teacher\nNew options available\n",
                    reply_markup=ReplyKeyboardRemove(),
                )
            else:
                await callback.message.answer("You are a student")

    await callback.answer()


@router.message(lambda msg: msg.text == "/teacher_menu", Command("teacher_menu"))
async def set_teacher_menu(message: Message) -> None:
    """Generate and show teacher menu."""
    keyboard: ReplyKeyboardMarkup = ButtonsKeyboard.generate_reply_keyboard(
        MenuKeyboard.teacher_commands_menu
    )
    await message.answer(text="Your available menu", reply_markup=keyboard)


@router.message(Text(text="Help💼"))
async def process_help_teacher_command(message: Message) -> None:
    """Execute command: teacher help. Show available teacher commands."""
    await message.answer(
        "\n".join(
            [
                f"{commands} - {description}"
                for commands, description in TEACHER_COMMANDS
            ]
        )
    )


@router.message(Text(text="Add information"))
async def process_register_teacher(message: Message, state: FSMContext) -> None:
    """Execute command: add information. Get info from user using FSM."""
    await state.set_state(TeacherRegister.name)

    await message.answer("Enter your full name")


@router.message(TeacherRegister.name)
async def process_get_name(message: Message, state: FSMContext) -> None:
    """Get name from user."""
    print(message.text.replace(" ", ""))
    if re.match(RegularExpressions.latin_and_spaces, message.text.replace(" ", "")):
        await state.update_data(name=message.text)

        DatabaseSettings.session.query(Teacher).filter(
            Teacher.user_id == message.from_user.id
        ).update(
            {
                Teacher.teacher_name: " ".join(
                    [el.capitalize() for el in message.text.split()]
                )
            }
        )
        DatabaseSettings.session.commit()
    else:
        await message.answer("Enter correct value")
        return

    await state.set_state(TeacherRegister.subjects)
    await message.answer("Enter your subjects like: math, informatics")


@router.message(TeacherRegister.subjects)
async def process_get_subjects(message: Message, state: FSMContext) -> None:
    """Get list of subjects from user."""
    if re.match(RegularExpressions.latin_spaces_commas, message.text):
        await state.update_data(name=message.text)
        # subjects = message.text.split(', ')

        DatabaseSettings.session.query(Teacher).filter(
            Teacher.user_id == message.from_user.id
        ).update({Teacher.subjects: message.text})
        DatabaseSettings.session.commit()
    else:
        await message.answer("Enter correct value")
        return
    await state.update_data(subjects=message.text)

    await state.set_state(TeacherRegister.groups)
    await message.answer("Enter your groups")


@router.message(TeacherRegister.groups)
async def process_get_address(message: Message, state: FSMContext) -> None:
    """Get list of groups from user. Clear state."""
    if message.text.isdigit() and len(message.text) <= 6:
        await state.update_data(groups=message.text)

        DatabaseSettings.session.query(Teacher).filter(
            Teacher.user_id == message.from_user.id
        ).update({Teacher.groups: message.text})
        DatabaseSettings.session.commit()
    else:
        await message.answer("Enter correct value")
        return

    await state.clear()
    await message.answer("Success!")


@router.message(Text(text="Analyse🔄"))
async def process_analyse_command(message: Message, state: FSMContext) -> None:
    """Execute command: analyse present students. Show result of neural network."""
    await state.set_state(UploadPhoto.photo)

    await message.answer("Upload photo to analyse")


@router.message(UploadPhoto.photo, F.content_type == ContentType.PHOTO)
async def process_analysed_photo(message: Message, bot: Bot, state: FSMContext) -> None:
    """Analyse uploaded photo."""
    await state.update_data(photo=message.photo)

    file: File = await bot.get_file(message.photo[-1].file_id)
    download_file: BinaryIO = await bot.download_file(file.file_path)

    await message.answer("🔄 Analysing 🔄")

    photo: PhotoAnalyser = PhotoAnalyser(download_file)
    await message.answer(photo.analyse_photo())

    data: dict[str, PhotoAnalyser] = {"photo": photo}
    await state.update_data(data=data)

    keyboard: InlineKeyboardMarkup = ButtonsKeyboard.generate_inline_keyboard(
        MenuKeyboard.yes_no_kb
    )
    await message.answer("Do you need to check data?", reply_markup=keyboard)


@router.callback_query(Text(text="No"))
async def process_help_command(callback: CallbackQuery) -> None:
    """Add info about passes to database."""
    await callback.answer()  # TODO: занести данные об посещении в бд. Пока не знаю в каком виде.


@router.callback_query(Text(text="Yes"))
async def callback_see_analysed_photo(
    callback: CallbackQuery, state: FSMContext, bot: Bot
) -> None:
    """Work if user need to check if data is right. Send photo with faces in rectangle."""
    data: dict[str, Any] = await state.get_data()
    data["photo"] = data["photo"]
    photo_path: Any = data["photo"].return_analysed_photo()

    await bot.send_photo(
        chat_id=callback.message.chat.id, photo=FSInputFile(photo_path)
    )

    new_yes_no_kb: list[str] = [x.lower() for x in MenuKeyboard.yes_no_kb]
    keyboard: InlineKeyboardMarkup = ButtonsKeyboard.generate_inline_keyboard(
        new_yes_no_kb
    )
    await callback.message.answer(
        "Here is your photo with analysed faces. Is everything okay?",
        reply_markup=keyboard,
    )

    await callback.answer()


@router.callback_query(Text(text="no"))
async def add_person_changed_name(callback: CallbackQuery, state: FSMContext) -> None:
    """Asks to write the user`s name with the appropriate changes."""
    await callback.message.answer("Add person name")
    await state.set_state(UploadPhoto.redone_persons)

    await callback.answer()


@router.message(UploadPhoto.redone_persons)
async def add_delete_persons(message: Message, state: FSMContext) -> None:
    """Asks what way of redone user needs."""
    await state.update_data(persons=message.text)

    # TODO: сделать проверку на наличие такого ученика в группе + на ввод данных
    # TODO: сделать коллбек "удалить или добавить человека"

    keyboard: InlineKeyboardMarkup = ButtonsKeyboard.generate_inline_keyboard(
        MenuKeyboard.delete_add_person
    )

    await message.answer("What you want to redone?", reply_markup=keyboard)

    validated_full_name = " ".join([el.capitalize() for el in message.text.split(" ")])

    data: dict[str, str] = {"person": validated_full_name}
    await state.update_data(data=data)

    await message.answer("You have added this person:")
    await message.answer(validated_full_name)


@router.callback_query(Text(text="Add"))
async def callback_add_persons(callback: CallbackQuery, state: FSMContext) -> None:
    """Remove the pass of selected user from database."""
    data: dict[str, Any] = await state.get_data()
    new_list: str = data["photo"].add_more_persons(data["person"])

    await callback.message.answer(new_list)  # TODO: в бд добавлять

    await callback.answer()


@router.callback_query(Text(text="Delete"))
async def callback_delete_persons(callback: CallbackQuery, state: FSMContext) -> None:
    """Add the pass of selected user to database."""
    data: dict[str, Any] = await state.get_data()
    new_list: str = data["photo"].delete_persons(data["person"])

    await callback.message.answer(new_list)  # TODO: из бд удалять

    await callback.answer()
