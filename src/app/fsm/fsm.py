from aiogram.fsm.state import State, StatesGroup


class StudentRegister(StatesGroup):
    name = State()
    telephone = State()
    address = State()


class TeacherRegister(StatesGroup):
    name = State()
    subjects = State()
    groups = State()


class UploadPhoto(StatesGroup):
    photo = State()
    redone_persons = State()


class StartMenu(StatesGroup):
    menu = State()
