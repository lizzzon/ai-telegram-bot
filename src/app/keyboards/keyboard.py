from aiogram import types
from aiogram.types import (
    InlineKeyboardButton,
    InlineKeyboardMarkup,
    KeyboardButton,
    ReplyKeyboardMarkup,
)
from aiogram.utils.keyboard import InlineKeyboardBuilder


class MenuKeyboard:
    """Class represents keyboard scopes."""

    user_commands_menu: list = [
        "Info📖",
        "Help🚑",
        "Website🕸️",
        "Role👩‍🏫",
        "Admin🔐💅🏻",
        "Teacher👩‍🏫",
        "Student👩‍🎓",
    ]

    role_commands_menu: list = [
        "👩‍🏫Teacher🧑‍🏫",
        "👩‍🎓Student🧑‍🎓",
    ]

    yes_no_kb: list = [
        "Yes",
        "No",
    ]

    delete_add_person: list = [
        "Delete",
        "Add",
    ]

    admin_commands_menu: list = [
        [
            KeyboardButton(text="Users👩🏼‍💻"),
            KeyboardButton(text="Help🗿"),
            KeyboardButton(text="Back to user😥"),
        ]
    ]

    teacher_commands_menu: list = [
        [
            KeyboardButton(text="Add information"),
            KeyboardButton(text="Analyse🔄"),
            KeyboardButton(text="Help💼"),
            KeyboardButton(text="Back to user😥"),
        ]
    ]

    student_commands_menu: list = [
        [
            KeyboardButton(text="Add info"),
            KeyboardButton(text="Help😫"),
            KeyboardButton(text="Show full info"),
            KeyboardButton(text="Back to user😥"),
        ]
    ]


class ButtonsKeyboard:
    """Class represents static methods which generate keyboard types."""

    @staticmethod
    def generate_inline_keyboard(
        buttons: list[str], adjust: int = 2
    ) -> InlineKeyboardMarkup:
        """
        Generate inline keyboard.

        Params:
        :param buttons: Text and Callback data
        :param adjust: Number of buttons in a row, defaults to 2
        """
        keyboard: InlineKeyboardBuilder = InlineKeyboardBuilder()

        for el in buttons:
            keyboard.add(types.InlineKeyboardButton(text=el, callback_data=el))

        keyboard.adjust(adjust)
        return keyboard.as_markup()

    @staticmethod
    def generate_reply_keyboard(commands: list[KeyboardButton]) -> ReplyKeyboardMarkup:
        """
        Generate reply keyboard.

        Params:
        :param commands: List of buttons in KeyboardButton class
        """
        keyboard: ReplyKeyboardMarkup = ReplyKeyboardMarkup(
            keyboard=commands, resize_keyboard=True
        )
        return keyboard

    @staticmethod
    def generate_url_button(text: str, url: str) -> InlineKeyboardMarkup:
        """
        Generate url button.

        Params:
        :param text: Button text
        :param url: Website link
        """
        url_button: list[list[InlineKeyboardButton]] = [
            [InlineKeyboardButton(text=text, url=url)]
        ]
        keyboard: InlineKeyboardMarkup = InlineKeyboardMarkup(
            inline_keyboard=url_button
        )
        return keyboard
