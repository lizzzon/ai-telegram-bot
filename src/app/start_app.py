import asyncio
import logging

from aiogram import Bot, Dispatcher
from aiogram.fsm.storage.redis import Redis, RedisStorage

from app.config.settings import get_settings
from app.handlers import (
    admin_handlers,
    other_handlers,
    student_handlers,
    teacher_handlers,
    user_handlers,
)
from app.utils.set_bot_commands import SetUserCommand


async def main() -> None:
    """Register dispatcher, bot. Include routers."""
    bot: Bot = Bot(token=get_settings().api_token)
    redis: Redis = Redis(host=get_settings().redis_host)
    storage: RedisStorage = RedisStorage(redis=redis)
    dp: Dispatcher = Dispatcher(storage=storage)

    await SetUserCommand.set_user_commands(bot)

    dp.include_routers(
        admin_handlers.router,
        user_handlers.router,
        student_handlers.router,
        teacher_handlers.router,
        other_handlers.router,
    )

    await bot.delete_webhook(drop_pending_updates=True)
    await dp.start_polling(bot, allowed_updates=dp.resolve_used_update_types())


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    try:
        asyncio.run(main())
    except (KeyboardInterrupt, SystemExit):
        print("Bot stopped!")
