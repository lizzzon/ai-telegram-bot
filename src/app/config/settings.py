import pathlib
from pathlib import Path
from typing import Any

from pydantic import BaseSettings
from pydantic.tools import lru_cache
from roboflow import Roboflow
from sqlalchemy import Engine, create_engine
from sqlalchemy.orm import Session, declarative_base, scoped_session, sessionmaker


class Settings(BaseSettings):
    """Class settings from .env file."""

    host: str
    password: str
    database: str
    port: str

    api_key: str
    ai_project_name: str

    api_token: str
    admin_id: dict

    redis_host: str
    redis_port: str

    class Config:
        """Class config from .env file."""

        env_file = Path(
            pathlib.Path(__file__).parent.parent.parent, "variables", "dev.env"
        )
        env_file_encoding = "utf-8"


@lru_cache
def get_settings() -> Settings:
    """Return settings from dev.env."""
    return Settings()


admin_id: list = []
for value in get_settings().admin_id.values():
    admin_id.append(value)


class RoboflowSettings:
    """Class consist settings to connect with Roboflow project model."""

    rf = Roboflow(api_key=get_settings().api_key)
    project = rf.workspace().project(get_settings().ai_project_name)
    model = project.version(3).model


class DatabaseSettings:
    """Class consist settings to connect with project Database."""

    engine: Engine = create_engine(
        f"postgresql+psycopg2://postgres:{get_settings().password}"
        f"@{get_settings().host}:{get_settings().port}/{get_settings().database}"
    )

    session: scoped_session[Session] = scoped_session(sessionmaker(bind=engine))
    Base: Any = declarative_base()
    Base.query = session.query_property()
    metadata: Any = Base.metadata
