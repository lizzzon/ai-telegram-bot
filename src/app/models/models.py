import enum
from typing import Any, Optional

from app.config.settings import DatabaseSettings

from sqlalchemy import Boolean, Column, Enum, ForeignKey, Integer, String


class Roles(str, enum.Enum):
    """Class type enum for user roles."""

    teacher = 'teacher'
    student = 'student'


class User(DatabaseSettings.Base):
    """Class represents the User model."""

    __tablename__: str = 'users'

    id: int = Column(Integer, primary_key=True)
    username: str = Column(String(120))
    name: str = Column(String(120))
    role: Any = Column(Enum(Roles), default=None)
    admin: bool = Column(Boolean, default=False)

    def __repr__(self) -> str:
        """Return string with all user fields."""
        return f'{self.user_id} {self.username} {self.name} {self.role} {self.admin}'


class Student(DatabaseSettings.Base):
    """Class represents the Student model."""

    __tablename__: str = 'students'

    id: int = Column(Integer, primary_key=True, autoincrement=True)
    user_id: int = Column(Integer, ForeignKey('users.id'))
    student_name: Optional[str] = Column(String(120), default=None)
    passes: Optional[int] = Column(Integer, default=None)
    average_mark: Optional[int] = Column(Integer, default=None)
    telephone: Optional[str] = Column(String(16), default=None)
    address: Optional[str] = Column(String(120), default=None)

    def __repr__(self) -> str:
        """Return string with all student fields."""
        return f'{self.id} {self.user_id} {self.student_name}' \
               f' {self.passes} {self.average_mark} {self.telephone} {self.address}'


class Teacher(DatabaseSettings.Base):
    """Class represents the Teacher model."""

    __tablename__: str = 'teachers'

    id: int = Column(Integer, primary_key=True, autoincrement=True)
    user_id: int = Column(Integer, ForeignKey('users.id'))
    teacher_name: str = Column(String(120))
    subjects: Optional[str] = Column(String(120), default=None)
    groups: str = Column(String(120))
    passes_id: int = Column(Integer, ForeignKey('passes.id'))

    def __repr__(self) -> str:
        """Return string with all teacher fields."""
        return f'{self.id} {self.user_id} {self.teacher_name}' \
               f' {self.subjects} {self.groups} {self.passes_id}'


class Pass(DatabaseSettings.Base):
    """Class represents the Pass model."""

    __tablename__: str = 'passes'

    id: int = Column(Integer, primary_key=True, autoincrement=True)
    count: int = Column(Integer, default=0)

    def __repr__(self) -> str:
        """Return string with all pass fields."""
        return f'{self.id} {self.count}'


DatabaseSettings.Base.metadata.create_all(DatabaseSettings.engine)
