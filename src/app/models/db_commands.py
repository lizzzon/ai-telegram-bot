from typing import Any

from aiogram.types import Message

from app.config.settings import DatabaseSettings, admin_id
from app.models.models import Student, Teacher, User

from sqlalchemy.exc import IntegrityError


class UserCommand:
    """Class represents for user model registration."""

    @staticmethod
    def register_user(message: Message) -> bool:
        """Register user. Add fields to database."""
        username: str = message.from_user.username if message.from_user.username else None
        user: User = User(
            id=int(message.from_user.id),
            username=username,
            name=message.from_user.full_name,
            admin=UserCommand.is_admin(int(message.from_user.id))
        )
        DatabaseSettings.session.add(user)

        try:
            DatabaseSettings.session.commit()
            return True
        except IntegrityError:
            DatabaseSettings.session.rollback()
            return False

    @staticmethod
    def select_user(user_id: int) -> Any:
        """
        Select user.

        Params:
        :param user_id: Sender id wrapped in int function
        """
        return DatabaseSettings.session.query(User).filter(User.id == user_id).first()

    @staticmethod
    def is_admin(user_id: int) -> bool:
        """
        Check user for admin rights.

        Params:
        :param user_id: Sender id wrapped in int function
        """
        return user_id in admin_id


class StudentCommand:
    """Class represents for student model registration."""

    @staticmethod
    def add_student(user_id: int) -> bool:
        """Add student."""
        student: Student = Student(
            user_id=user_id,
        )
        DatabaseSettings.session.add(student)

        try:
            DatabaseSettings.session.commit()
            return True
        except IntegrityError:
            DatabaseSettings.session.rollback()
            return False


class TeacherCommand:
    """Class represents for teacher model registration."""

    @staticmethod
    def add_teacher(user_id: int, teacher_name: str) -> bool:
        """Add teacher."""
        teacher: Teacher = Teacher(
            user_id=user_id,
            teacher_name=teacher_name,
        )
        DatabaseSettings.session.add(teacher)

        try:
            DatabaseSettings.session.commit()
            return True
        except IntegrityError:
            DatabaseSettings.session.rollback()
            return False


class PassCommand:
    """Class represents for pass model registration."""

    pass


class UpdateCommandUser:
    """Class includes update functions."""

    @staticmethod
    def update_table(txt: str, callback_id: int) -> None:
        """
        Update database fields.

        Params:
        :param txt: Update field value to txt
        :param callback_id: Sender id from message
        """
        DatabaseSettings.session.query(User).filter(User.id == callback_id).update({User.role: txt})
        DatabaseSettings.session.commit()
