"""'initial'

Revision ID: 03448d5ed9d5
Revises: 
Create Date: 2023-06-19 08:59:42.667482

"""
from alembic import op
import sqlalchemy as sa

from app.models.models import Roles

# revision identifiers, used by Alembic.
revision = '03448d5ed9d5'
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        'users',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('username', sa.String(120), nullable=False),
        sa.Column('name', sa.String(120), nullable=False),
        sa.Column('role', sa.Enum(Roles), nullable=False, default=None),
        sa.Column('admin', sa.Boolean, nullable=False, default=False),
    )

    op.create_table(
        'students',
        sa.Column('id', sa.Integer, primary_key=True, autoincrement=True),
        sa.Column('user_id', sa.Integer, sa.ForeignKey('users.id')),
        sa.Column('student_name', sa.String(120), nullable=True, default=None),
        sa.Column('passes', sa.Integer, default=None, nullable=True),
        sa.Column('average_mark', sa.Integer, default=None, nullable=True),
        sa.Column('telephone', sa.String(16), default=None, nullable=True),
        sa.Column('address', sa.String(120), default=None, nullable=True),
    )

    op.create_table(
        'teachers',
        sa.Column('id', sa.Integer, primary_key=True, autoincrement=True),
        sa.Column('user_id', sa.Integer, sa.ForeignKey('users.id')),
        sa.Column('teacher_name', sa.String(120), nullable=False, default=None),
        sa.Column('subjects', sa.String(120), default=None, nullable=True),
        sa.Column('groups', sa.String(120), nullable=False),
        sa.Column('passes_id', sa.Integer, sa.ForeignKey('passes_id'))
    )

    op.create_table(
        'passes',
        sa.Column('id', sa.Integer, primary_key=True, autoincrement=True),
        sa.Column('count', sa.Integer, default=0),
    )


def downgrade() -> None:
    op.drop_table('users')
    op.drop_table('students')
    op.drop_table('teachers')
    op.drop_table('passes')
