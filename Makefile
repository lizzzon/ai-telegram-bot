build:
	docker-compose -f docker-compose.yaml --env-file variables/dev.env up -d

build-dockerfile:
	docker build -t elizavetaborisyonok14122002:tg_bot .
	docker images

test:
	pytest src/tests

migrate:
	cd src && alembic revision --autogenerate -m 'initial' && cd ..