FROM python:3.11 as base

ENV PYTHONUNBUFFERED=1
ENV PYTHONDONTWRITEBYTECODE=1

WORKDIR /app
COPY pyproject.toml poetry.lock /app/

COPY ./app .

CMD ["python", "src/app/start_app.py"]
